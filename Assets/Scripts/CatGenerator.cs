﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CatGenerator : MonoBehaviour
{
    //public string[] names;
    public Material[] markings;
    public GameObject[] traits;

    public CatData GenerateCat()
    {
        CatData cd = new CatData();
        //cd.name = names[Random.Range(0,names.Length)];

        //// Starsign
        cd.starsign = (Starsign)Random.Range(0,12);

        //// Markings
        int numMark;
        if (Random.Range(0, 100) < 5)
        {
            numMark = 0;
        }
        else
        {
            numMark = Random.Range(1, 4);
        }
        string[] pickedMarks = new string[numMark];
        Color[] markCol = new Color[numMark];
        for (int i = 0; i < numMark; i++)
        {
            pickedMarks[i] = markings[Random.Range(0, markings.Length)].name;
            markCol[i] = new Color(Random.value, Random.value, Random.value);
        }
        cd.markings = pickedMarks;
        cd.markingColors = markCol;

        //// Traits
        string[] pickedTraits = new string[3];
        Color[] traitCol = new Color[3];
        int x = 0;
        for (int i = 0; i < 3; i++)
        {
            if(Random.Range(0,100) < 25)
            {
                pickedTraits[x] = traits[Random.Range(0, traits.Length)].name;
                traitCol[x] = new Color(Random.value, Random.value, Random.value);
                x++;
            }
        }
        cd.traits= pickedTraits;
        cd.traitColors = traitCol;

        //// Base Color
        float v = Random.Range(0.0F, 0.2F);
        cd.baseColor = new Color(v, v, v);
        cd.eyeColor = new Color(Random.value, Random.value, Random.value);



        return cd;
    }

    public string CatDataToText(CatData cat)
    {
        string str = "";
        if(cat.gender == 0)
        {
            str += "Female" + '\n';
        }
        else
        {
            str += "Male" + '\n';
        }

        str += cat.starsign.ToString() + '\n';

        for (int i = 0; i < cat.markings.Length; i++)
        {
            str += cat.markings[i] + '\n';
        }

        for (int i = 0; i < cat.traits.Length; i++)
        {
            if(traits[i] != null)
            {
                str += cat.traits[i] + '\n';
            }
        }
        return str;
    }

}

[Serializable]
public class CatData
{
    public string name;
    public int gender;
    public Starsign starsign;

    public Color baseColor;
    public Color eyeColor;

    public string[] markings;
    public Color[] markingColors;

    public string[] traits;
    public Color[] traitColors;

}

[Serializable]
public enum Starsign
{
    Aries,
    Taurus,
    Gemini,
    Cancer,
    Leo,
    Virgo,
    Libra,
    Scorpio,
    Sagittarius,
    Capricorn,
    Aquarius,
    Pisces
}