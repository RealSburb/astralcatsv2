﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorController : MonoBehaviour
{
    public static GeneratorController control;

    private CatGenerator gen;
    public CatInstance catModel;

    // happens before start
    void Awake()
    {
        // create Singleton
        if (control == null) // First time running
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gen = this.GetComponent<CatGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Generate a cat, convert it to text, then show it in a text object.
    /// </summary>
    /// <param name="t"></param>
    public void RandomTextCat(Text t)
    {
        CatData cd = gen.GenerateCat();
        catModel.setCatData(cd);
        t.text = gen.CatDataToText(cd);
    }

    public Material getMaterial(string mark)
    {
        foreach (Material m in gen.markings)
        {
            if(m.name == mark)
            {
                return m;
            }
        }
        return null;
    }

}
