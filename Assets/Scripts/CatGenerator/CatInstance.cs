﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatInstance : MonoBehaviour
{
    public GameObject model;
    public GameObject obj;
    public CatData data;

    /// <summary>
    /// Apply CatData to this instance
    /// </summary>
    /// <param name="inDat"></param>
    public void setCatData(CatData inDat)
    {
        data = inDat;
        ApplyMaterials();
        ApplyTraits();
    }

    void ApplyMaterials()
    {
        SkinnedMeshRenderer meshRend = model.GetComponent<SkinnedMeshRenderer>();

        //0 base    //1 eyes    //2Nose/Paws/Ears
        Material[] defMaterials = meshRend.materials;
        // Set Base colors
        defMaterials[0].color = data.baseColor;    // base
        defMaterials[1].color = data.eyeColor;     // eyes
        //defMaterials[1].SetColor("_EmissionColor", data.eyeColor);
        defMaterials[2].color = data.eyeColor;     // Nose/Paws/Ears

        // Get Marking keys and colors
        string[] keys = data.markings;
        Color[] colors = data.markingColors;

        // Default materials
        int offset = 3; // so the base color, eyes, and ears don't get overwritten
        int numMaterials = offset + keys.Length;
        Material[] newMaterials = new Material[numMaterials];
        newMaterials[0] = defMaterials[0];
        newMaterials[1] = defMaterials[1];
        newMaterials[2] = defMaterials[2];

        // Get Marks from array
        //int inc = 2;
        for (int i = 0; i < keys.Length; i++)
        {
            // get material from GC
            Material m = GeneratorController.control.getMaterial(keys[i]);
            if (m != null) // returns null if marking could not be found. Happens with pre-alpha cats
            {
                // set color
                m.color = colors[i];
                // add material to list
                newMaterials[i + offset] = m;
            }
        }

        // Assign
        meshRend.materials = newMaterials;
    }

    public void ApplyTraits()
    {
        // Remove Last Cat's Traits
        ResetModel();
        // turn on traits marked
        string[] traitkeys = data.traits;
        Color[] colors = data.traitColors;
        int i = 0;
        foreach (string key in traitkeys)
        {
            if (key != null)
            {
                // Get Trait and activate
                GameObject trait = obj.transform.Find(key).gameObject;
                trait.SetActive(true);
                // Set Color
                if(trait.GetComponent<MeshRenderer>() != null)
                {
                    MeshRenderer rend = trait.GetComponent<MeshRenderer>();
                    // Get default materials from renderer
                    Material[] defMaterials = rend.materials;
                    // Set new color
                    defMaterials[0].color = colors[i];
                    // Set the Trait's renderer to the new material
                    rend.materials = defMaterials;
                    i++;
                }
                else
                {
                    SkinnedMeshRenderer rend = trait.GetComponent<SkinnedMeshRenderer>();
                    Material[] defMaterials = rend.materials;
                    // Set new color
                    defMaterials[0].color = colors[i];
                    // Set the Trait's renderer to the new material
                    rend.materials = defMaterials;
                    i++;
                }


            }
        }

    }

    private void ResetModel()
    {
        //Reset Traits
        foreach(Transform t in obj.transform)
        {
            GameObject child = t.gameObject;
            if(child.name != "ACat" && child.name != "Armature")
            {
                child.SetActive(false);
            }
        }
    }
}
